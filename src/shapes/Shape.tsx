import moment from 'moment';
import React from 'react';
import { Rect, Transformer, Circle, Text } from 'react-konva';

const Shape = ({ shapeProps, rebarrItemProps, isSelected, onSelect, onChange }) => {
  let shapeRef = React.useRef();
  let trRef = React.useRef();

  React.useEffect(() => {
    if (isSelected) {
      // we need to attach transformer manually
      trRef.current.nodes([shapeRef.current]);
      trRef.current.getLayer().batchDraw();
    }
  }, [isSelected]);

  const generateShape = () => {
    let shape;

    if (!shapeProps.markToDelete) {
      if (shapeProps.shape_type === 'square') {
        shape = (
          <Rect
            id={String(shapeProps.id)}
            name={shapeProps.shape_type}
            onClick={onSelect}
            onTap={onSelect}
            ref={shapeRef}
            x={shapeProps.position_x}
            y={shapeProps.position_y}
            width={shapeProps.width}
            height={shapeProps.height}
            fill={shapeProps.color_code}
            opacity={0.6}
            draggable
            onDragEnd={e => {
              onChange({
                ...shapeProps,
                x: e.target.x(),
                y: e.target.y(),
              });
            }}
            onTransformEnd={e => {
              // transformer is changing scale of the node
              // and NOT its width or height
              // but in the store we have only width and height
              // to match the data better we will reset scale on transform end
              const node = shapeRef.current;
              const scaleX = node.scaleX();
              const scaleY = node.scaleY();

              // we will reset it back
              node.scaleX(1);
              node.scaleY(1);
              onChange({
                ...shapeProps,
                x: node.x(),
                y: node.y(),
                position_x: node.x(),
                position_y: node.y(),
                // set minimal value
                width: Math.max(5, node.width() * scaleX),
                height: Math.max(node.height() * scaleY),
              });
            }}
          />
        );
      } else if (shapeProps.shape_type === 'circle') {
        shape = (
          <Circle
            id={String(shapeProps.id)}
            name={shapeProps.shape_type}
            onClick={onSelect}
            ref={shapeRef}
            {...shapeProps}
            draggable
            x={shapeProps.position_x}
            y={shapeProps.position_y}
            fill={shapeProps.color_code}
            radius={shapeProps.size}
            width={shapeProps.width}
            height={shapeProps.height}
            opacity={0.6}
            onDragEnd={e => {
              onChange({
                ...shapeProps,
                x: e.target.x(),
                y: e.target.y(),
              });
            }}
            onTransformEnd={e => {
              const node = shapeRef.current;
              const scaleX = node.scaleX();
              const scaleY = node.scaleY();

              node.scaleX(1);
              node.scaleY(1);
              onChange({
                ...shapeProps,
                x: node.x(),
                y: node.y(),
                position_x: node.x(),
                position_y: node.y(),
                width: Math.max(5, node.width() * scaleX),
                height: Math.max(node.height() * scaleY),
                size: Math.max(parseFloat(shapeProps.size) * scaleX),
              });
            }}
          />
        );
      } else if (shapeProps.shape_type === 'text' || shapeProps.shape_type === 'markdown_text') {
        let text = shapeProps.text;
        if (shapeProps.shape_type === 'markdown_text') {
          rebarrItemProps.forEach(itemProp => {
            if (itemProp === 'id') {
              text = text.replace(`%${itemProp}%`, shapeProps.item_id);
            } else if (itemProp === 'code') {
              let strReplace = `%${itemProp}%`;
              text = text.replace(strReplace, shapeProps.code);
            } else if (itemProp === 'status') {
              text = text.replace(`%${itemProp}%`, shapeProps.status);
            } else if (itemProp === 'receivedDate') {
              let strReplace = `%${itemProp}%`;
              text = text.replace(
                strReplace,
                moment(shapeProps.received_date).format('MMM, DD YYYY HH:mm:ss'),
              );
            }
          });
        }

        shape = (
          <Text
            id={String(shapeProps.id)}
            name={shapeProps.shape_type}
            text={text}
            padding={10}
            fontSize={shapeProps.size}
            onClick={onSelect}
            onTap={onSelect}
            ref={shapeRef}
            x={shapeProps.position_x}
            y={shapeProps.position_y}
            fill="#ffffff"
            draggable
            onDragEnd={e => {
              onChange({
                ...shapeProps,
                x: e.target.x(),
                y: e.target.y(),
              });
            }}
            onTransformEnd={e => {
              // transformer is changing scale of the node
              // and NOT its width or height
              // but in the store we have only width and height
              // to match the data better we will reset scale on transform end
              const node = shapeRef.current;
              const scaleX = node.scaleX();
              const scaleY = node.scaleY();

              // we will reset it back
              node.scaleX(1);
              node.scaleY(1);
              onChange({
                ...shapeProps,
                x: node.x(),
                y: node.y(),
                position_x: node.x(),
                position_y: node.y(),
                // set minimal value
                width: Math.max(5, node.width() * scaleX),
                height: Math.max(node.height() * scaleY),
                size: shapeProps.size * scaleX,
              });
            }}
          />
        );
      }
    }

    return shape;
  };

  return (
    <React.Fragment>
      {generateShape()}
      {isSelected && (
        <Transformer
          ref={trRef}
          boundBoxFunc={(oldBox, newBox) => {
            // limit resize
            if (newBox.width < 5 || newBox.height < 5) {
              return oldBox;
            }
            return newBox;
          }}
        />
      )}
    </React.Fragment>
  );
};

export { Shape };
