import React from 'react';
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBan, faSave } from '@fortawesome/free-solid-svg-icons';

const ModifyDialog = ({ props, onClose, open }) => {
  const handleClose = () => {
    // onClose(selectedValue);
    open = false;
  };

  return (
    <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={open}>
      <DialogTitle id="simple-dialog-title">Set backup account</DialogTitle>
      <DialogContent>
        <DialogContentText>This is for modify shapes</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="secondary">
          Cancel &nbsp; <FontAwesomeIcon icon={faBan} />
        </Button>
        <Button onClick={handleClose} color="primary">
          Cancel &nbsp; <FontAwesomeIcon icon={faSave} />
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export { ModifyDialog };
