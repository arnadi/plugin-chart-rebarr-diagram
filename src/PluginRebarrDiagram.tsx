/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License") you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import React, { useEffect, createRef, useState } from 'react'
import { PluginRebarrDiagramProps } from './types'

import { Stage, Layer } from 'react-konva'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faBan,
  faEdit,
  faExclamationTriangle,
  faHighlighter,
  faImage,
  faSave,
  faTextWidth,
  faTimes,
  faTrash,
  faUpload,
} from '@fortawesome/free-solid-svg-icons'
import { faSquare, faCircle } from '@fortawesome/free-regular-svg-icons'

import {
  Button,
  ButtonGroup,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  CircularProgress,
  TextField,
  MenuItem,
  Avatar,
  Select,
  FormControl,
  InputLabel,
  Grid,
  Snackbar,
  IconButton,
} from '@material-ui/core'

import axios from 'axios'

import { Shape } from './shapes/Shape'
import { styled } from '@superset-ui/core'
import moment from 'moment'
import imageNotFound from './images/image_not_found.png'

// The following Styles component is a <div> element, which has been styled using Emotion
// For docs, visit https://emotion.sh/docs/styled

// Theming variables are provided for your use via a ThemeProvider
// imported from @superset-ui/core. For variables available, please visit
// https://github.com/apache-superset/superset-ui/blob/master/packages/superset-ui-core/src/style/index.ts

// const Styles = styled.div<PluginRebarrDiagramStylesProps>`
//   background-color: ${({ theme }) => theme.colors.secondary.light2}
//   padding: ${({ theme }) => theme.gridUnit * 4}px
//   border-radius: ${({ theme }) => theme.gridUnit * 2}px
//   height: ${({ height }) => height}
//   width: ${({ width }) => width}
//   overflow-y: scroll

//   h3 {
//     /* You can use your props to control CSS! */
//     font-size: ${({ theme, headerFontSize }) => theme.typography.sizes[headerFontSize]}
//     font-weight: ${({ theme, boldText }) => theme.typography.weights[boldText ? 'bold' : 'normal']}
//   }
// `

export default function PluginRebarrDiagram(props: PluginRebarrDiagramProps) {
  // height and width are the height and width of the DOM element as it exists in the dashboard.
  // There is also a `data` prop, which is, of course, your DATA 🎉
  const { data, height, width } = props

  const [datas, setDatas] = useState([])
  const [rerender, setRerender] = useState(false)
  const [shapeId, setShapeId] = useState(null)
  const [openDialog, setOpenDialog] = useState(false)
  const [backgroundImage, setBackgroundImage] = useState(null)
  const [rebarrItems, setRebarrItems] = useState([])
  const [imageName, setImageName] = useState('')
  const [dialogLoading, setDialogLoading] = useState(false)
  const [selectedFile, setSelectedFile] = useState(null)
  const [openModifyDialog, setOpenModifyDialog] = useState(false)
  const [itemId, setItemId] = useState('')
  const [selectedItem, setSelectedItem] = useState(null)
  const [selectedShape, setSelectedShape] = useState(null)
  const [textValue, setTextValue] = useState('')
  const [isMarkdownText, setIsMarkdownText] = useState(false)
  var [markdownText, setMarkdownText] = useState('')
  const [propNames, setPropNames] = useState([])
  const [selectedPropName, setSelectedPropName] = useState('')
  const [selectedPropValue, setSelectedPropValue] = useState('None')
  const [saveConfirmDialog, setSaveConfirmDialog] = useState(false)
  const [openAlert, setOpenAlert] = useState(false)
  const [isEditMode, setIsEditMode] = useState(false)

  const rootElem = createRef<HTMLDivElement>()
  const backendUrl = 'http://47.241.220.43'

  // Often, you just want to get a hold of the DOM and go nuts.
  // Here, you can do that with createRef, and the useEffect hook.
  useEffect(() => {
    const root = rootElem.current as HTMLElement

    initializeData()
    getBackgroundImage()
  }, [])

  const AvatarLabel = styled.div`
    display: flex
    align-items: center
  `

  const getBackgroundImage = async () => {
    await axios
      .get(`${backendUrl}/rebarr/background-image`)
      .then(response => {
        if (response.status === 200) {
          setBackgroundImage(response.data.data.filePath)
          setImageName(response.data.data.fileName)
        }
      })
      .catch(error => {
        console.log(error)
      })
  }

  const initializeData = async () => {
    await axios
      .get(`${backendUrl}/rebarr-item/list`)
      .then(response => {
        if (response.status === 200) {
          setRebarrItems(response.data.data)

          const propNames = [
            'id', 'code', 'status', 'receivedDate'
          ]
          setPropNames(propNames)

          const dataSet = data
          dataSet.forEach(shapeData => {
            if (shapeData.item_id !== null) {
              const foundItem = response.data.data.find(rebarrItem => {
                return shapeData.item_id === rebarrItem.id
              })
              if (foundItem) {
                shapeData.status = foundItem.rebarrStatus.name
                shapeData.color_code = foundItem.rebarrStatus.colorCode
                shapeData.received_date = foundItem.receivedDate
              }
            }
            shapeData.isDragging = false
            shapeData.isNew = shapeData.isNew !== undefined ? shapeData.isNew : false
            shapeData.markToDelete = false
          })
          setDatas(dataSet)
        }
      })
      .catch(error => {
        console.log(error)
      })
  }

  const addShape = (shapeType: string) => {
    const rebarrObjects = datas
    let width = 0
    let height = 0
    let size = 0
    let text = null
    let colorCode = '#ffffff'
    if (shapeType === 'circle') {
      size = 50
    } else if (shapeType === 'square') {
      width = 100
      height = 100
    } else if (shapeType === 'text') {
      width = 150
      height = 50
      size = 16
      colorCode = '#ffffff'
      text = 'This is sample text'
    }

    let newShape = {
      id:
        Math.max.apply(
          Math,
          datas.map(function (obj) {
            return obj.id
          }),
        ) + 1,
      position_x: 100,
      position_y: 100,
      width: width,
      height: height,
      size: size,
      shape_type: shapeType,
      text: text,
      item_id: null,
      created_at: null,
      updated_at: null,
      code: null,
      received_date: null,
      status: null,
      color_code: colorCode,
      count: 1,
      isDragging: false,
      isNew: true,
    }
    rebarrObjects.push(newShape)

    setDatas(rebarrObjects)
    setRerender(!rerender)
  }

  const saveObjectChanges = async () => {
    const rebarrObjects = datas
    await axios
      .put(`${backendUrl}/rebarr-join-table/update`, rebarrObjects)
      .then(response => {
        if (response.status == 200) {
          setSaveConfirmDialog(false)
          setOpenAlert(true)
          setIsEditMode(false)
        }
      })
  }

  const handleDeleteObject = () => {
    const dataSet = datas
    let foundIndex = dataSet.findIndex(data => {
      return data.id === parseInt(selectedShape.id)
    })

    if (foundIndex !== -1) {
      dataSet[foundIndex].markToDelete = true
      setDatas(dataSet)
      handleModifyClose()
    }
  }

  const onChangeFile = event => {
    setSelectedFile(event.target.files[0])
  }

  const changeBackgroundImage = async () => {
    setDialogLoading(true)
    const formData = new FormData()
    formData.append('new_image', selectedFile, selectedFile.name)
    formData.append('previous_image', imageName)

    await axios
      .post(`${backendUrl}/rebarr/change-background-image`, formData)
      .then(response => {
        setBackgroundImage(response.data.data.filePath)
        setImageName(response.data.data.fileName)
        setDialogLoading(false)
        handleClose()
      })
      .catch(error => {
        console.log(error)
        setDialogLoading(false)
      })
  }

  const handleStageDoubleClick = event => {
    if (
      event.target.attrs.name === 'square' ||
      event.target.attrs.name === 'circle' ||
      event.target.attrs.name === 'text' ||
      event.target.attrs.name === 'markdown_text'
    ) {
      setShapeId(event.target.attrs.id)
      const shape = datas.find(d => {
        return d.id === parseInt(event.target.attrs.id)
      })
      setSelectedShape(shape)

      if (event.target.attrs.name === 'markdown_text') {
        const foundItem = rebarrItems.find(rebarrItem => {
          return rebarrItem.id === parseInt(shape.item_id)
        })
        setItemId(shape.item_id)
        setSelectedItem(foundItem)
        setIsMarkdownText(true)
        setMarkdownText(shape.text)
      }

      setOpenModifyDialog(true)
    }
  }

  const handleClickOpen = () => {
    setOpenDialog(true)
  }

  const handleClose = () => {
    setOpenDialog(false)
    setSelectedFile(null)
  }

  const checkDeselect = e => {
    // deselect when clicked on empty area
    const clickedOnEmpty = e.target === e.target.getStage()
    if (clickedOnEmpty) {
      setShapeId(null)
    }
  }

  const openMarkdownDialog = () => {
    setIsMarkdownText(true)
    setSelectedShape(null)
    setOpenModifyDialog(true)
  }

  const handleModifyClose = () => {
    setOpenModifyDialog(false)
    setSelectedItem(null)
    setTextValue('')
    setIsMarkdownText(false)
    setSelectedPropName('')
    setSelectedPropValue('')
    setMarkdownText('')
    setItemId('')
    setSelectedItem(null)
  }

  const handleItemChange = event => {
    if (event.target.name === 'select-related-item') {
      setItemId(event.target.value)
      const currentItem = rebarrItems.find(rebarrItem => {
        return rebarrItem.id === parseInt(event.target.value)
      })

      setSelectedItem(currentItem)
    } else if (event.target.name === 'select-prop-name') {
      setSelectedPropName(String(event.target.value))
    }
  }

  const handleMarkdownTextChange = event => {
    setMarkdownText(event.target.value)
  }

  const handleTextAreaChange = event => {
    setTextValue(event.target.value)
  }

  const getCurrentShapeCode = () => {
    if (selectedShape !== null) {
      return selectedShape.code !== null ? selectedShape.code : 'None'
    }
    return 'None'
  }

  const generateDialogContent = (): any => {
    if (selectedShape.shape_type === 'square' || selectedShape.shape_type === 'circle') {
      return (
        <div>
          <DialogContentText>Current Shape Color</DialogContentText>
          {selectedShape && (
            <Avatar
              style={{ backgroundColor: selectedShape.color_code, marginBottom: 15, opacity: 0.6 }}
            >
              &nbsp;
            </Avatar>
          )}
          <TextField
            disabled
            label="Current Selected Item"
            defaultValue={getCurrentShapeCode()}
            fullWidth={true}
          />
          <TextField
            id="select-related-item"
            select
            label="Select Related Item"
            value={itemId}
            onChange={handleItemChange}
            fullWidth={true}
            name="select-related-item"
          >
            {rebarrItems.map(item => (
              <MenuItem key={item.id} value={item.id}>
                <AvatarLabel>
                  <Avatar style={{ backgroundColor: item.rebarrStatus.colorCode, opacity: 0.6 }}>
                    &nbsp;
                  </Avatar>{' '}
                  &nbsp;
                  {item.code}
                </AvatarLabel>
              </MenuItem>
            ))}
          </TextField>
        </div>
      )
    } else if (selectedShape.shape_type === 'text') {
      return (
        <div>
          <textarea
            style={{ width: '100%' }}
            rows={4}
            defaultValue={selectedShape.text}
            onChange={handleTextAreaChange}
          ></textarea>
        </div>
      )
    }

    return <div></div>
  }

  const generateMarkdownTextContent = () => {
    return (
      <div>
        <FormControl style={{ marginRight: 15, minWidth: 160 }}>
          <InputLabel id="select-related-item">Select Related Item</InputLabel>
          <Select
            id="select-related-item"
            autoWidth
            value={itemId}
            onChange={handleItemChange}
            name="select-related-item"
          >
            {rebarrItems.map(item => (
              <MenuItem key={item.id} value={item.id}>
                <AvatarLabel>{item.code}</AvatarLabel>
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl style={{ marginRight: 15, minWidth: 120 }}>
          <InputLabel>Select Property</InputLabel>
          <Select
            id="select-prop-name"
            autoWidth
            label="Select Property Name"
            value={selectedPropName}
            onChange={handleItemChange}
            name="select-prop-name"
            disabled={ !selectedItem ? true : false }
          >
            {propNames.map((propName, index) => (
              <MenuItem key={index} value={propName}>
                {propName}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl style={{ marginTop: 15 }}>
          <Button 
            variant="contained" 
            color="primary" 
            onClick={() => addValueToMarkdownText()}
            disabled={ selectedPropName === '' ? true : false }>
            Add to text
          </Button>
        </FormControl>
        {selectedPropName !== '' && generatePropValue()}

        <h4>Text :</h4>
        <div>
          <textarea
            style={{ width: '100%' }}
            value={markdownText}
            rows={5}
            onChange={handleMarkdownTextChange}
          ></textarea>
        </div>
      </div>
    )
  }

  const generatePropValue = () => {
    const items = rebarrItems
    const foundItem = items.find(item => {
      return item.id === parseInt(itemId)
    })
    return <div>Value: {foundItem[selectedPropName]}</div>
  }

  const addValueToMarkdownText = () => {
    setMarkdownText((markdownText += `%${selectedPropName}%`))
  }

  const saveItemChanges = () => {
    const dataSet = datas
    if (markdownText.trim() !== '') {
      if (selectedShape === null) {
        let newShape = {
          id:
            Math.max.apply(
              Math,
              datas.map(function (obj) {
                return obj.id
              }),
            ) + 1,
          position_x: 100,
          position_y: 100,
          width: width,
          height: height,
          size: 16,
          shape_type: 'markdown_text',
          text: markdownText,
          item_id: itemId,
          created_at: moment().format('YYYY-MM-DD hh:mm:ss'),
          updated_at: moment().format('YYYY-MM-DD hh:mm:ss'),
          code: selectedItem.code,
          received_date: selectedItem.receivedDate,
          status: selectedItem.rebarrStatus.name,
          color_code: '#ffffff',
          count: 1,
          isDragging: false,
          isNew: true,
        }

        dataSet.push(newShape)
      }
    }

    if (selectedShape) {
      const foundDataIndex = dataSet.findIndex(data => {
        return data.id === selectedShape.id
      })

      const currentSelectedItem = rebarrItems.find(rebarrItem => {
        return rebarrItem.id === itemId
      })

      if (foundDataIndex !== -1) {
        if (
          dataSet[foundDataIndex].shape_type === 'square' ||
          dataSet[foundDataIndex].shape_type === 'circle'
        ) {
          if (itemId) {
            dataSet[foundDataIndex].item_id = itemId
            dataSet[foundDataIndex].code = currentSelectedItem.code
            dataSet[foundDataIndex].color_code = currentSelectedItem.rebarrStatus.colorCode
          }
        } else if (dataSet[foundDataIndex].shape_type === 'markdown_text') {
          if (markdownText.trim() !== '') {
            dataSet[foundDataIndex].text = markdownText
            dataSet[foundDataIndex].item_id = currentSelectedItem.id
            dataSet[foundDataIndex].code = currentSelectedItem.code
            dataSet[foundDataIndex].received_date = currentSelectedItem.receivedDate
            dataSet[foundDataIndex].color_code = currentSelectedItem.rebarrStatus.colorCode
            dataSet[foundDataIndex].status = currentSelectedItem.rebarrStatus.name
          }
        } else if (dataSet[foundDataIndex].shape_type === 'text') {
          if (textValue.trim() !== '') {
            dataSet[foundDataIndex].text = textValue
          }
        }
      }
    }

    setDatas(dataSet)

    handleModifyClose()
  }

  return (
    <div>
      { isEditMode &&
        <ButtonGroup color="primary" aria-label="outlined primary button group">
          <Button onClick={() => handleClickOpen()}>
            Change Image &nbsp; <FontAwesomeIcon icon={faImage} />
          </Button>
          <Button onClick={() => addShape('square')}>
            Add &nbsp; <FontAwesomeIcon icon={faSquare} />
          </Button>
          <Button onClick={() => addShape('circle')}>
            Add &nbsp; <FontAwesomeIcon icon={faCircle} />
          </Button>
          <Button onClick={() => addShape('text')}>
            Add Text &nbsp; <FontAwesomeIcon icon={faTextWidth} />
          </Button>
          <Button onClick={() => openMarkdownDialog()}>
            Add Markdown Text &nbsp; <FontAwesomeIcon icon={faHighlighter} />
          </Button>
          <Button onClick={() => setSaveConfirmDialog(true)}>
            Save &nbsp; <FontAwesomeIcon icon={faSave} />
          </Button>
        </ButtonGroup>
      }

      { !isEditMode &&
        <Button variant="contained" color="primary" onClick={() => setIsEditMode(true)}>
          Edit &nbsp; <FontAwesomeIcon icon={faEdit} />
        </Button>
      }

      <div
        style={{
          position: 'absolute',
          top: 35,
          left: 0,
          height: '100%',
          width: '100%',
          backgroundImage: `url(${ backgroundImage !== null ? backgroundImage : imageNotFound})`,
          backgroundSize: '100% 95%',
          backgroundRepeat: 'no-repeat',
          backgroundPosition: '0% 0%',
        }}
      >
        <Stage
          width={width}
          height={height}
          onDblClick={handleStageDoubleClick}
          onMouseDown={checkDeselect}
          onTouchStart={checkDeselect}
        >
          <Layer>
            {datas.map(function (value, index) {
              return (
                <Shape
                  key={index}
                  shapeProps={value}
                  rebarrItemProps={propNames}
                  isSelected={value.id === shapeId}
                  onSelect={() => setShapeId(value.id)}
                  onChange={newAttrs => {
                    const rects = datas.slice()
                    rects[index] = newAttrs
                    rects[index].position_x = newAttrs.x
                    rects[index].position_y = newAttrs.y
                    setDatas(rects)
                  }}
                />
              )
            })}
          </Layer>
        </Stage>
      </div>

      <Dialog open={openDialog} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title" color="primary" style={{ fontSize: '2em' }}>
          Info
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            Do you want to change the background image? <br />
            Click the button below to upload your image
          </DialogContentText>

          <input
            type="file"
            id="backgroundImage"
            name="avatar"
            accept="image/png, image/jpeg"
            onChange={onChangeFile}
          ></input>
        </DialogContent>
        <DialogActions>
          {!dialogLoading && (
            <Button onClick={handleClose} color="secondary">
              Cancel &nbsp; <FontAwesomeIcon icon={faBan} />
            </Button>
          )}
          {!dialogLoading && (
            <Button
              onClick={() => changeBackgroundImage()}
              color="primary"
              disabled={selectedFile === null ? true : false}
            >
              Upload &nbsp; <FontAwesomeIcon icon={faUpload} />
            </Button>
          )}
          {dialogLoading && <CircularProgress />}
        </DialogActions>
      </Dialog>

      {/* Modify Dialog */}
      <Dialog
        aria-labelledby="simple-dialog-title"
        open={openModifyDialog}
        fullWidth={true}
        maxWidth={'sm'}
      >
        <DialogTitle id="simple-dialog-title">
          {selectedShape && !isMarkdownText && 'Modify Object'}
          {!selectedShape && isMarkdownText && 'Add Markdown Text'}
          {selectedShape && isMarkdownText && 'Modify Markdown Text'}
        </DialogTitle>
        <DialogContent>
          {selectedShape && generateDialogContent()}
          {isMarkdownText && generateMarkdownTextContent()}
        </DialogContent>
        <DialogActions>
          <Grid justify="space-between" container style={{ paddingLeft: '1%', paddingRight: '1%' }}>
            <Grid item>
              {selectedShape && (
                <Button color="secondary" onClick={() => handleDeleteObject()}>
                  Delete Shape &nbsp; <FontAwesomeIcon icon={faTrash} />
                </Button>
              )}
            </Grid>
            <Grid item>
              <Button onClick={handleModifyClose} color="secondary">
                Cancel &nbsp; <FontAwesomeIcon icon={faBan} />
              </Button>
              <Button onClick={saveItemChanges} color="primary">
                Save Changes &nbsp; <FontAwesomeIcon icon={faSave} />
              </Button>
            </Grid>
          </Grid>
        </DialogActions>
      </Dialog>

      <Dialog
        aria-labelledby="save-confirm-dialog"
        open={saveConfirmDialog}
        fullWidth={true}
        maxWidth={'sm'}
      >
        <DialogTitle id="simple-dialog-title">
          Confirmation &nbsp; <FontAwesomeIcon icon={faExclamationTriangle} />
        </DialogTitle>
        <DialogContent>
          Are you sure want to save this data ? <br/>
          Please make sure all data was correct
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setSaveConfirmDialog(false) } color="secondary">
            Cancel &nbsp; <FontAwesomeIcon icon={faBan} />
          </Button>
          <Button onClick={() => saveObjectChanges()} color="primary">
            Save Changes &nbsp; <FontAwesomeIcon icon={faSave} />
          </Button>
        </DialogActions>
      </Dialog>

      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        open={openAlert}
        autoHideDuration={4000}
        onClose={() => setOpenAlert(false)}
        message="Data has been saved"
        action={
          <React.Fragment>
            <IconButton size="small" aria-label="close" color="inherit" onClick={() => setOpenAlert(false)}>
              <FontAwesomeIcon icon={faTimes} />
            </IconButton>
          </React.Fragment>
        }
      />

    </div>
  )
}
