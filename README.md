## @superset-ui/plugin-chart-plugin-rebarr-diagram

This plugin provides Plugin Rebarr Diagram for Superset.

### Usage

Configure `key`, which can be any `string`, and register the plugin. This `key` will be used to
lookup this chart throughout the app.

```js
import PluginRebarrDiagramChartPlugin from '@superset-ui/plugin-chart-rebarr-diagram';

new PluginRebarrDiagramChartPlugin().configure({ key: 'plugin-rebarr-diagram' }).register();
```

Then use it via `SuperChart`. See
[storybook](https://apache-superset.github.io/superset-ui/?selectedKind=plugin-chart-plugin-rebarr-diagram)
for more details.

```js
<SuperChart
  chartType="plugin-rebarr-diagram"
  width={600}
  height={600}
  formData={...}
  queriesData={[{
    data: {...},
  }]}
/>
```

### File structure generated

```
├── package.json
├── README.md
├── tsconfig.json
├── src
│   ├── PluginRebarrDiagram.tsx
│   ├── images
│   │   └── thumbnail.png
│   ├── index.ts
│   ├── plugin
│   │   ├── buildQuery.ts
│   │   ├── controlPanel.ts
│   │   ├── index.ts
│   │   └── transformProps.ts
│   └── types.ts
├── test
│   └── index.test.ts
└── types
    └── external.d.ts
```
